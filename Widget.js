 
define([
        "dojo/_base/declare",
        "jimu/BaseWidget",
		"esri/map",
		"esri/toolbars/draw",
		"esri/graphic",

		"esri/graphicsUtils",
		"esri/symbols/SimpleMarkerSymbol",
		"esri/symbols/SimpleLineSymbol",
		"esri/symbols/SimpleFillSymbol",
		"esri/Color",

		"esri/tasks/Geoprocessor",
		"esri/tasks/FeatureSet",
		"esri/tasks/LinearUnit",
		"dojo/ready",
		"dojo/parser",

		"dojo/on",
		"dojo/_base/array",
		"esri/tasks/QueryTask",
		"esri/tasks/query",
		"esri/arcgis/utils",

		"esri/symbols/SimpleMarkerSymbol",
		"dijit/registry",
        "dijit/Menu",
        "dijit/MenuItem",
		"esri/request",

		"dojox/gfx/fx",
		"dojox/charting/Chart",
		"dojox/charting/plot2d/Pie",
		"dojox/charting/action2d/Highlight",
		"dojox/charting/action2d/MoveSlice",

		"dojox/charting/action2d/Tooltip",
		"dojox/charting/themes/MiamiNice",
		"dojox/charting/widget/Legend",
		"dijit/Dialog",
		"dijit/form/TextBox",

		"dijit/form/Button",
		"esri/layers/StreamLayer",
         "dojo/dom",
		"esri/InfoTemplate",
         "esri/tasks/GeometryService",
         "esri/tasks/BufferParameters",
          "esri/layers/FeatureLayer",
         "esri/config", "dojo/_base/Color"

],
    function (
        b,
        c,
		Map,
		Draw,
		Graphic,

		graphicsUtils,
		SimpleMarkerSymbol,
		SimpleLineSymbol,
		SimpleFillSymbol,
		Color,

		Geoprocessor,
		FeatureSet,
		LinearUnit,
		ready,
		parser,

		on,
		array,
		QueryTask,
		Query,
		arcgisUtils,

		SimpleMarkerSymbol,
		registry,
        Menu,
		MenuItem,
		esriRequest,

		dojox_gfx_fx,
		Chart,
		Pie,
		Highlight,
		MoveSlice,

		Tooltip,
		MiamiNice,
		Legend,
		Dialog,
		TextBox,

		Button,
		StreamLayer,
        dom,
		InfoTemplate,
        GeometryService,
        BufferParameters,
        FeatureLayer,
         esriConfig, Color
    ) {
    return b([c], {
        baseClass: "jimu-widget-demo",
        postCreate: function () {
            this.inherited(arguments);
            that2 = this;
            that2.MAPS;
            that2.MAPS = this.map;
        },
        row_onclick :function()
        {
            //alert('123');
            var iddamchay = this.children[6].innerText.trim();
            var query = new Query();

            query.outFields = ["*"];
         //   query.orderByFields = ["OBJECTID"];
            query.returnGeometry = true;
            query.where = "OBJECTID=" + iddamchay;
            var task = new QueryTask("https://gis1061.esrivn.net/server/rest/services/PCCC/Call_info/FeatureServer/0");
            task.execute(query, function (set) {
                for (var i = 0; i < set.features.length; i++) {
                    var geo = set.features[i].geometry;
                    that2.MAPS.centerAndZoom(geo, 15);
                }
            }, function (err) {
                alert(err.stack);
            });
        },
         startup: function () {
            this.inherited(arguments);
           
        },
         onOpen: function () {
             var p = this.getPanel();
             p.setPosition({ left: 850, top: 0, right: 0, bottom: 0, width: "180mm", height: "100mm", zIndex: 100 });
             
             //p.width = 400;
             //p.height = 400;
         },
        onClose: function () { console.log("onClose") },
        onMinimize: function () { console.log("onMinimize") },
        onMaximize: function () { console.log("onMaximize") },
        onSignIn: function (a) { console.log("onSignIn") },
        onSignOut: function () { console.log("onSignOut") },
        showVertexCount: function (a) {   }
    })
});